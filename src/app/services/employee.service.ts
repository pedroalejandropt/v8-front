import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  url = `${environment.api}`;

  constructor(private _http: HttpClient) { }

  /**
    * This function get all employees.
    * 
    * @returns Promise of the list of employees.
  */
  fetchEmployees = () => { return this._http.get(`${this.url}employee`).toPromise() }

  /**
    * This function get all positions.
    * 
    * @returns Promise of the list of positions.
  */
   fetchPositions = () => { return this._http.get(`${this.url}position`).toPromise() }

   /**
    * This function get all divisions.
    * 
    * @returns Promise of the list of divisions.
  */
  fetchDivisions = () => { return this._http.get(`${this.url}division`).toPromise() }

  /**
    * This function get all divisions.
    * 
    * @returns Promise of the list of divisions.
  */
   fetchOffices = () => { return this._http.get(`${this.url}office`).toPromise() }

}
