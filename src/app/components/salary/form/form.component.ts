import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { SalaryService } from 'src/app/services/salary.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Input() employees;
  @Input() offices;
  @Input() divisions;
  @Input() positions;

  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() reloadEvent = new EventEmitter<any>();

  filteredOptions: Observable<string[]>;

  employee: any;
  employeeName = new FormControl();

  office: any;
  officeName: string;

  division: any;
  divisionName: string;

  position: any;
  positionName: string;

  beginDate: any;

  checked: boolean = false

  months: number[] = [1,2,3,4,5,6,7,8,9,10,11,12]
  years: number[] = [2018,2019,2020,2021]

  records = [{ month: '', year: '', grade: '', baseSalary: '', productionBonus: '', compensationBonus: '', commision: '', contributions: '' }];

  error = '';

  constructor(private _service: SalaryService) { }

  ngOnInit() {
    this.filteredOptions = this.employeeName.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.employees
    .filter(option => 
      option.employeeName.toLowerCase().includes(filterValue) || 
      option.employeeSurname.toLowerCase().includes(filterValue) ||
      (option.employeeName.toLowerCase() + ' ' + option.employeeSurname.toLowerCase()).includes(filterValue));
  }

  check = () => this.checked = !this.checked;

  cleanInfo = () => {
    this.officeName = '';
    this.divisionName = '';
    this.positionName = '';
    this.beginDate = null;
  }

  addRecord = () =>
    this.records.push({ month: '', year: '', grade: '', baseSalary: '', productionBonus: '', compensationBonus: '', commision: '', contributions: '' });
  
  removeRecord = (record, index) => this.records.splice(index, 1)

  cancel = () => this.closeEvent.emit(false);

  assignEmployee = (employee) => { this.employee = employee; this.employeeName.setValue(employee.employeeName + ' ' + employee.employeeSurname)}
  assignOffice = (office) => this.office = office;
  assignDivision = (division) => this.division = division;
  assignPosition = (position) => this.position = position;

  addSalary = () => {
    this.records.forEach(record => {
      let history = {
        'employee': {
          'id': this.employee.id,
          'identificationNumber': this.employee.identificationNumber,
          'employeeCode': this.employee.employeeCode,
          'employeeName': this.employee.employeeName,
          'employeeSurname': this.employee.employeeSurname,
          'birthday': this.employee.birthday
        },
        'salary': {
          'baseSalary': record.baseSalary,
          'productionBonus': record.productionBonus,
          'compensationBonus': record.compensationBonus, 
          'commision': record.commision, 
          'contributions': record.contributions
        },
        'month': record.month,
        'year': record.year,
        'grade': record.grade
      }

      if (this.checked) { 
        history['officeId'] = this.office.id;
        history['divisionId'] = this.division.id;
        history['positionId'] = this.position.id;
        history['beginDate'] = this.beginDate;
      }
      this._service.postSalary(history).then(res => { this.closeEvent.emit(false); this.reloadEvent.emit(); }).catch(ex => this.error = 'Verify all inputs are complete')
    })
  }

}
