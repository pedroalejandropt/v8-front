import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  title: string = 'List of Employee';
  header: any = {'identificationNumber': 'Identification Number', 'employeeCode': 'Code', 'employeeName': 'Name', 'employeeSurname': 'Surname'}

  employees: any[] = [
    /* { firstName: 'A', middleName: 'B', firstLastName: 'C', secondLastName: 'D', email: '', password: '1', role: 1 },
    { firstName: 'B', middleName: 'C', firstLastName: 'D', secondLastName: 'E', email: '', password: '2', role: 1 },
    { firstName: 'C', middleName: 'D', firstLastName: 'E', secondLastName: 'F', email: '', password: '3', role: 1 },
    { firstName: 'D', middleName: 'E', firstLastName: 'F', secondLastName: 'G', email: '', password: '4', role: 1 } */
  ];

  actions: any[] = []

  loading: boolean = true;

  constructor(private _service: EmployeeService) { }

  ngOnInit() {
    this._service.fetchEmployees().then((res: any) => { this.employees = res; this.loading = false; })
  }

}
