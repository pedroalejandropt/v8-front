import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  url = `${environment.api}salary`;

  constructor(private _http: HttpClient) { }

  /**
    * This function get all salaries.
    * 
    * @returns Promise of the list of salaries.
  */
  fetchSalaries = () => { return this._http.get(`${this.url}/history`).toPromise() }

  /**
    * This function post a salary.
    * 
    * @returns Promise of post salary.
  */
  postSalary = (record) => { return this._http.post(`${this.url}`, record).toPromise() }

  /**
    * This function get a bonus.
    * 
    * @returns Promise of get bonus.
  */
   getBonus = (id) => { return this._http.get(`${this.url}/last/${id}`).toPromise() }

}
