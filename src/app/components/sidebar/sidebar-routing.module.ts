import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar.component';

const routes: Routes = [
  { 
    path: '', 
    component: SidebarComponent,
    children: [
      {
        path: '',
        loadChildren: '../../pages/principal/principal.module#PrincipalModule'
      },
      {
        path: 'employee',
        loadChildren: '../../pages/employee/employee.module#EmployeeModule'
      },
      {
        path: 'salary',
        loadChildren: '../../pages/salary/salary.module#SalaryModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SidebarRoutingModule { }
