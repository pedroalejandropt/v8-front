import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';
import { SalaryService } from 'src/app/services/salary.service';

@Component({
  selector: 'app-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.css']
})
export class SalaryComponent implements OnInit {

  title: string = 'List of Salaries';
  header: any = {
    'identificationNumber': 'Identification Number', 'employeeCode': 'Code', 
    'employeeName': 'Full Name', 'month': 'Month', 'year': 'Year', 'totalSalary': 'Total Salary', 
    'birthday': 'Birthday', 'beginDate': 'Begin Date', 'officeName': 'Office', 'divisionName': 'Division', 'positionName': 'Position'
  }

  options: any[] = [];
  salaries: any[] = [];
  employees: any[] = [];
  
  positions: any[] = [];
  position: string;

  divisions: any[] = [];
  division: string;

  offices: any[] = [];
  office: string;

  grade: any;

  actions: any[] = []

  loading: boolean = true;

  form: boolean = false;

  constructor(private _service: SalaryService, private _serviceEmployee: EmployeeService) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData = () => {
    this._service.fetchSalaries().then((res: any) => { this.salaries = res; this.options = res; this.loading = false; })
    this._serviceEmployee.fetchPositions().then((res: any) => { this.positions = res; })
    this._serviceEmployee.fetchDivisions().then((res: any) => { this.divisions = res; })
    this._serviceEmployee.fetchOffices().then((res: any) => { this.offices = res; })
    this._serviceEmployee.fetchEmployees().then((res: any) => { this.employees = res; })
  }

  showForm = () => this.form = true;
  closeForm = (value: boolean) => this.form = value;

  filter = async () => {
    await setTimeout(async() => {
      this.loading = await true;
    }, 10);
    await setTimeout(async() => {
      ((this.office) && (this.position) && (this.grade)) ? this.options = await this.salaries.filter(s => s.officeName.includes(this.office) && s.positionName.includes(this.position) && s.grade == this.grade) :
      ((this.office) && (this.grade)) ? this.options = await this.salaries.filter(s => s.officeName.includes(this.office) && s.grade == this.grade) :
      ((this.position) && (this.grade)) ? this.options = await this.salaries.filter(s => s.positionName.includes(this.position) && s.grade == (this.grade)) :
      ((this.office) && (this.position)) ? this.options = await this.salaries.filter(s => s.officeName.includes(this.office) && s.positionName.includes(this.position)) :
      (this.office) ? this.options = await this.salaries.filter(s => s.officeName.includes(this.office)) :
      (this.position) ? this.options = await this.salaries.filter(s => s.positionName.includes(this.position)) :
      (this.grade) ? this.options = await this.salaries.filter(s => s.grade == (this.grade)) : null
      this.loading = await false
    }, 100);
  }

  clear = async () => {
    await setTimeout(async() => {
      this.loading = await true;
      this.office = '';
      this.position = '';
      this.grade = '';
    }, 10);
    await setTimeout(async() => {
      this.options = this.salaries;
      this.loading = await false
    }, 100);
  }
  

}
