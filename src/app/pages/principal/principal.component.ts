import { Component, OnInit } from '@angular/core';
import { SalaryService } from 'src/app/services/salary.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  bonus: any = '';
  employeeCode: string = '';

  constructor(private _service: SalaryService) { }

  ngOnInit() {}

  calculateBonus = () => this._service.getBonus(this.employeeCode).then(c => this.bonus = c)

}
