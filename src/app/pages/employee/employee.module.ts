import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeComponent } from './employee.component';
import { TableModule } from 'src/app/components/table/table.module';
import { MatButtonModule, MatProgressSpinnerModule } from '@angular/material';

@NgModule({
  declarations: [EmployeeComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    TableModule,
    MatProgressSpinnerModule,
    MatButtonModule
  ]
})
export class EmployeeModule { }
